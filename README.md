# README #

### What is this repository for? ###
These projects contain the server for the language profile page application.

### How do I get set up? ###

**Node.js is required to run this server**

To set up this server with your local database, configure the properties in the createConnection method to correspond with your MySQL database

To start the server, navigate to the folder in a terminal window. First, install the dependencies by using `npm install` and then simply run `node app.js`.