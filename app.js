const express = require('express');
const mysql = require('mysql');

const db = mysql.createConnection({
  host      : 'localhost',
  user      : 'root',
  password  : 'Am@zing_P@$$w0rd',
  database  : 'wycliffe'    
})

db.connect(err => {
  if(err) {
    throw err;
  } else {
    console.log('MySQL Connected');
  }
})

const app = express();

app.listen(3001, () => {
  console.log('Server started on port 3001');
})